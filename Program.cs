﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Program
    {
        const double StartX = 10.3;
        const double dx = 0.7;

        static double Function(double x)
        {
            double x1 = 2.76 * x;
            double x2 = 0.5 * x;
            double xx = Math.Pow(x1, 3) * Math.Pow(x2, 5);
            double sinn = Math.Pow(Math.Sin(xx), 2);
            return 23 * sinn + 2 * x1 + Math.Cos(x1 * x2);
        }
        static void Main(string[] args)
        {
            double[] arr = new double[10];

            double x = StartX;
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                arr[i] = Function(x);
                x += dx;
            }

            Array.Sort(arr);
            Array.Reverse(arr);

            Console.WriteLine("Вiдсортованi за спаданням значення масиву: ");

            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                Console.WriteLine("arr[{0:00}] = {1:0.0000}", i, arr[i]);
            }

            double aMin = arr[arr.GetUpperBound(0)];
            double aMax = arr[arr.GetLowerBound(0)];

            double aAvg = 0;
            for (int i = arr.GetLowerBound(0); i <= arr.GetUpperBound(0); i++)
            {
                aAvg += arr[i];
            }
            aAvg = aAvg / arr.GetLength(0);

            Console.WriteLine("Мiнiмальне значення масиву: {0:0.0000}", aMin);
            Console.WriteLine("Максимальне значення масиву: {0:0.0000}", aMax);
            Console.WriteLine("Середнє значення масиву: {0:0.0000}", aAvg);


            double sum = aMin;

            double aMIN = aMin + ((aMin / 100) * 10);
            for (double i = aMin; i <= aMIN; i++)
            {
                sum += i;
            }

            Console.WriteLine("Сума елементів масиву, які знаходяться в діапазоні aMin... aMin + 10%aMin: {0:0.0000} ", sum);
            Console.ReadKey(true);
        }
    }
}